// [SECTION] MongoDB Aggregation
/*
	- Used to generate, manipulate data and perform operations to create filtered results that heps us to analuze the data.

*/

	// Using the aggregate method:
	/*
		- The "$match"
		- Syntax:
			{$match: {field:value}}
		- The "$group" is used to group elements together and field-value pairs teh data from the grouped elements.
		- Syntax:
			{$group: {_id: "value", fieldResult: "valueResult"}};

		- using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the ottal amount of stock for all suppliers found.
		- Syntax
			db.collectionName.aggregate([{$match:{fieldA: valueA}}, {$group: {id: "value", field: "valueB"}}]) 
	*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total:{$sum: "$stock"}}}
	]);

	// Field projection with aggregation
	/*
		- The "$project" can be used when aggregating data to include/exlcude fields from the returned result.

		- Syntax:
			{$project: {field 1/0}}
	*/

	db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total:{$sum: "$stock"}}},
	{$project:{_id:0}}
	]);

	// Sorting aggregated results
	/*
		- The "$sort" can be used to change the order of the aggregated result
		-Syntax:
			{$sort: {field:1}}
			1 -> lowest to highest
			-1 -> highest to lowest

	*/

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", 
		total:{$sum: "$stock"}}},
		{$sort: {total:-1}}
	]);

	// Aggregating results based on array fields

	/*
		- the "$unwind" deconstructs an array fireld from a collection/field with an array value to output a result.
		- Syntax:
			- {$unwind: field}

	*/

	db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds:{$sum: 1}}}
	]);

	// [SECTION] Other Aggregate stages

	// $count all yellow fruits

	db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$count: "Yellow Fruits"}
		]);

	// $avg gets the average value of stock

	// $min & $max
	db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group:{_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
	]);